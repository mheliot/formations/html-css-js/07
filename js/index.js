'use strict';

(function(document) {

    var addButton = document.querySelector('#addBtn')
    var addForm = document.querySelector('#addForm')
    var addModale = document.querySelector('#addModale')
    var cancelAddBtn = document.querySelector('#cancelAddBtn')

    function addArticle(data) {
        var articleElt = document.createElement('article')
        articleElt.classList.add('article')        

        var cbxElt = document.createElement('div')
        cbxElt.classList.add('col', 'col--cbx')
        cbxElt.innerHTML = '<input type="checkbox" checked="checked" />'
        articleElt.appendChild(cbxElt)      

        var codeElt = document.createElement('div')
        codeElt.classList.add('col')
        codeElt.innerText = data.get('code')
        articleElt.appendChild(codeElt)      

        var titleElt = document.createElement('div')
        titleElt.classList.add('col', 'col--main')
        titleElt.innerHTML = '<h2 class="title-2">' + data.get('title') + '</h2>'
        articleElt.appendChild(titleElt)      

        var authorElt = document.createElement('div')
        authorElt.classList.add('col')
        authorElt.innerHTML = '<h3 class="title-3">' + data.get('author') + '</h3>'
        articleElt.appendChild(authorElt)    

        var createdAtElt = document.createElement('div')
        createdAtElt.classList.add('col')
        var createdAtDate = new Date()
        createdAtElt.innerText = createdAtDate.toLocaleDateString("fr-FR")
        articleElt.appendChild(createdAtElt)    

        var priceElt = document.createElement('div')
        priceElt.classList.add('col')
        var price = Number(data.get('price')).toFixed(2)
        priceElt.innerText = price + ' €'
        articleElt.appendChild(priceElt) 

        var liElt = document.createElement('li')
        liElt.classList.add('list__content__item')
        liElt.appendChild(articleElt)
        
        document.querySelector('#articleList .list__content').appendChild(liElt)
    }

    function closeModale() {
        addModale.close()
    }

    function openModale() {
        addModale.showModal()
    }
    
    addButton.addEventListener('click', () => {
        openModale()
    })

    addForm.addEventListener('submit', event => {
        event.preventDefault()
        closeModale()
        var data = new FormData(event.target)
        addArticle(data)
        event.target.reset()
    })

    cancelAddBtn.addEventListener('click', () => {
        closeModale()
    })

}(document))